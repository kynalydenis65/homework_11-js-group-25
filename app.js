document.addEventListener('DOMContentLoaded', function () {
    const passwordForm = document.querySelector('.password-form');

    passwordForm.addEventListener('click', (e) => {
        if (e.target.classList.contains('icon-password')) {
            const icon = e.target;
            const input = icon.previousElementSibling;
            togglePasswordVisibility(input, icon);
        }
    });

    passwordForm.addEventListener('submit', (e) => {
        e.preventDefault();

        const passwordInputs = passwordForm.querySelectorAll('input[type="password"]');
        const password1 = passwordInputs[0].value;
        const password2 = passwordInputs[1].value;

        if (password1 == password2) {
            alert('You are welcome');
        } else {
            const errorText = document.createElement('p');
            errorText.textContent = 'Потрібно ввести однакові значення';
            errorText.style.color = 'red';
            passwordForm.insertBefore(errorText, passwordForm.lastChild);
        }
    });

    function togglePasswordVisibility(input, icon) {
        if (input.type === 'password') {
            input.type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else {
            input.type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    }
});
